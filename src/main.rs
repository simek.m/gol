use std::{fmt, thread};

const BOARD_SIZE: usize = 50;
const DELAY: u32 = 100;

struct Board {
    cells: [[bool; BOARD_SIZE]; BOARD_SIZE]
}

impl Board {
    fn new() -> Self {
        Self {
            cells: [[false; BOARD_SIZE]; BOARD_SIZE]
        }
    }

    fn tick(&self) -> Self {
        let mut new_state = Self::new();

        for (y, row) in self.cells.iter().enumerate() {
            for (x, cell) in row.iter().enumerate() {
                new_state.cells[y][x] = self.should_live(x as i32, y as i32);
            }
        }
        new_state
    }

    fn cell(&self, x: i32, y: i32) -> bool {
        if (x >= 0 && y >= 0) &&
            (x <= (self.cells.len() as i32 - 1)
             && y <= (self.cells.len() as i32 -1)) {
            return self.cells[y as usize][x as usize];
        }
        false
    }

    fn should_live(&self, x: i32, y: i32) -> bool {
        let num_neighbors = self.neighbors(x, y);
        let cell = self.cell(x, y);

        match (cell, num_neighbors) {
            (true, 2...3) => true,
            (false, 3) => true,
            _  => false,
        }
    }

    fn neighbors(&self, x: i32, y: i32) -> u8 {

        (self.cell(x+1, y) as u8)
        + (self.cell(x-1, y) as u8)
        + (self.cell(x, y+1) as u8)
        + (self.cell(x, y-1) as u8)
        + (self.cell(x+1, y+1) as u8)
        + (self.cell(x-1, y-1) as u8)
        + (self.cell(x+1, y-1) as u8)
        + (self.cell(x-1, y+1) as u8)
    }
}

impl fmt::Debug for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut output = String::new();
        for row in self.cells.iter() {
            for cell in row.iter() {
                output.push_str(
                    if *cell { "x" } else { " " }
                );
            }
            output.push_str("\n");
        }
        write!(f, "{}", output)
    }
}

fn main() {
    let mut board = Board::new();
    board.cells[0] = [true; BOARD_SIZE];

    loop {
        println!("{:?}", board);
        board = board.tick();
        thread::sleep_ms(DELAY);
    }
}
